package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import fr.afcepf.al34.project1.spring.springboutique.entity.CustomerGender;

public interface CustomerGenderService {
	
	CustomerGender saveInBase(CustomerGender cg);
	List<CustomerGender> getAllCustomerGenders();
}
