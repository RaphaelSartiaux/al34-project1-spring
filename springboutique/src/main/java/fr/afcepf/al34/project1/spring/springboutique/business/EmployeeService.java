package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import fr.afcepf.al34.project1.spring.springboutique.entity.Credentials;
import fr.afcepf.al34.project1.spring.springboutique.entity.Employee;

public interface EmployeeService {

	Employee searchById(Integer id);
	Employee saveInBase(Employee employee);
	List<Employee> findWithCredentials(Credentials credentials);
}
