package fr.afcepf.al34.project1.spring.springboutique.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
@Entity
@Table(name = "batch")
public class Batch implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private int quantity;

    @Column(name = "addition_date")
    private LocalDate additionDate;

    @ManyToOne
	@JoinColumn(referencedColumnName="id")
    private Product product;
    
    @ManyToOne
    @JoinColumn(referencedColumnName="id")
    private Cart cart;
    
    @ManyToOne
	@JoinColumn(referencedColumnName="id")
    private Promotion promotion;

	public Batch(int quantity, LocalDate additionDate, Product product, Cart cart) {
		super();
		this.quantity = quantity;
		this.additionDate = additionDate;
		this.product = product;
		this.cart = cart;
	}

	public Batch(LocalDate additionDate, Cart cart) {
		super();
		this.additionDate = additionDate;
		this.cart = cart;
	}
	
    
    


}
