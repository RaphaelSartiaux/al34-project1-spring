package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import fr.afcepf.al34.project1.spring.springboutique.entity.Credentials;

public interface CredentialsService {

	Credentials searchById(Integer id);
	Credentials saveInBase(Credentials credentials);
	Credentials findWithLogin(String login);
	
}
