package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.project1.spring.springboutique.dao.CustomerGenderDao;
import fr.afcepf.al34.project1.spring.springboutique.dao.InventoryDao;
import fr.afcepf.al34.project1.spring.springboutique.dao.PhotoDao;
import fr.afcepf.al34.project1.spring.springboutique.dao.ProductDao;
import fr.afcepf.al34.project1.spring.springboutique.dao.SportProductDao;
import fr.afcepf.al34.project1.spring.springboutique.entity.Category;
import fr.afcepf.al34.project1.spring.springboutique.entity.CustomerGender;
import fr.afcepf.al34.project1.spring.springboutique.entity.Inventory;
import fr.afcepf.al34.project1.spring.springboutique.entity.Photo;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;
import fr.afcepf.al34.project1.spring.springboutique.entity.Promotion;
import fr.afcepf.al34.project1.spring.springboutique.entity.Sport;
import fr.afcepf.al34.project1.spring.springboutique.entity.SportProduct;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductDao productDao;
	
	@Autowired
	PhotoDao photoDao;
	
	@Autowired
	InventoryDao inventoryDao;
	
	@Autowired
	SportProductDao sportProductDao;
	
	@Override
	public Product saveInBase(Product p) {
		return productDao.save(p);
	}

	@Override
	public List<Product> getAllProducts() {
		List<Product> result = (List<Product>) productDao.findAll();
		for (Product product : result) {
			product.getPhotos().size();
		}
		return result;
	}

	@Override
	public Photo saveInBase(Photo photo) {
		return photoDao.save(photo);
	}

	@Override
	public List<Inventory> getAllInventories() {
		
		return (List<Inventory>) inventoryDao.findAll();
	}
	
	@Override
	public List<Product> findWithCustomerGenderName(String genderName) {
		List<Product> result = (List<Product>) productDao.findByCustomerGenderName(genderName);
		for (Product product : result) {
			product.getPhotos().size();
		}
		return result;
	}

	@Override
	public List<Product> findWithCategory(Integer categoryId) {
		List<Product> result = (List<Product>) productDao.findByCategoryId(categoryId);
		for (Product product : result) {
			product.getPhotos().size();
		}
		return result;
	}

	@Override
	public List<Product> findWithCustomerGenderNameAndCategoryName(String customerGenderName, String categoryName) {
		List<Product> result = (List<Product>) productDao.
				findByCustomerGenderNameAndCategoryName(customerGenderName, categoryName);
		for (Product product : result) {
			product.getPhotos().size();
		}
		return result;
	}
	
	public List<Product> findWithCustomerGenderAndCategory(Integer customerGenderId,Integer categoryId) {
		List<Product> result = (List<Product>) productDao.
				findByCustomerGenderIdAndCategoryId(customerGenderId, categoryId);
		for (Product product : result) {
			product.getPhotos().size();
		}
		return result;

	}

	@Override
	public List<Product> findWithCustomerGenderAndSport(Integer customerGenderId, Integer sportId) {
		List<Product> products = (List<Product>) productDao.findByCustomerGenderId(customerGenderId);
		List<SportProduct> SportProduct =(List<SportProduct>) sportProductDao.findAll();
		List<Product> myProducts = new ArrayList<Product>();
		for(SportProduct sp : SportProduct) {
			if(sp.getSport().getId().equals(sportId) && sp.getProduct().getCustomerGender().getId().equals(customerGenderId)){
				myProducts.add(sp.getProduct());
			}
		}
		for (Product product : myProducts) {
			product.getPhotos().size();
		}
		return myProducts;
	}

	@Override
	public List<Product> findWithGender(Integer genderId) {
		List<Product> result = (List<Product>) productDao.findByCustomerGenderId(genderId);
		for (Product product : result) {
			product.getPhotos().size();
		}
		return result;
	}



}
