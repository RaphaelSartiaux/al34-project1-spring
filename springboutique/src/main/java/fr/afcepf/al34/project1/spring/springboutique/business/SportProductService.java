package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import fr.afcepf.al34.project1.spring.springboutique.entity.Inventory;
import fr.afcepf.al34.project1.spring.springboutique.entity.SportProduct;
import fr.afcepf.al34.project1.spring.springboutique.entity.Type;


public interface SportProductService {

	SportProduct saveInBase(SportProduct sportProduct);

}
