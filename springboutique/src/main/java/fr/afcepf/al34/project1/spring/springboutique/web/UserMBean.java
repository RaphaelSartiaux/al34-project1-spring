package fr.afcepf.al34.project1.spring.springboutique.web;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import fr.afcepf.al34.project1.spring.springboutique.business.AuthenticationManager;
import fr.afcepf.al34.project1.spring.springboutique.business.CartService;
import fr.afcepf.al34.project1.spring.springboutique.business.ClientService;
import fr.afcepf.al34.project1.spring.springboutique.business.CredentialsService;
import fr.afcepf.al34.project1.spring.springboutique.business.EmployeeService;
import fr.afcepf.al34.project1.spring.springboutique.business.UserService;
import fr.afcepf.al34.project1.spring.springboutique.entity.Batch;
import fr.afcepf.al34.project1.spring.springboutique.entity.Cart;
import fr.afcepf.al34.project1.spring.springboutique.entity.Client;
import fr.afcepf.al34.project1.spring.springboutique.entity.Credentials;
import fr.afcepf.al34.project1.spring.springboutique.entity.Employee;
import fr.afcepf.al34.project1.spring.springboutique.entity.Gender;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ManagedBean
@SessionScoped
@Getter @Setter @NoArgsConstructor
public class UserMBean {
	//Pour Démo
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private String email;
	//Fin Démo
	
	private String login;
	
	@Size(min=6)
	private String clearPassword;
	
	private String message;
	
	private Client client = new Client();
	
	@Email
	private Employee employee;
	
	private Gender gender;
	List<Gender> genders;

	@Inject
	private CredentialsService credentialsService;

	@Inject
	private ClientService clientService;

	@Inject
	private EmployeeService employeeService;

	@Inject
	private UserService userService;

	@Inject
	private CartService cartService;
	
	@PostConstruct
	public void init() {
		this.genders = userService.getAllGenders();
	}

	public String doConnectionEmployee() {
		Credentials credentials = credentialsService.findWithLogin(this.login);
		if(credentials != null) {
			Credentials cred = credentials;
			try {
				if(AuthenticationManager.authenticate(clearPassword, cred)) {
					employee = employeeService.findWithCredentials(cred).get(0);
				} else {
					message = "indentifiants incorrects";
					return "employeeHome.xhtml?faces-redirect=true";
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			message = "indentifiants incorrects";
			return "login.xhtml";
		}
		return "employeeHome.xhtml?faces-redirect=true";
	}

	public String doCreateAccount() {
		
		Credentials cred = new Credentials(login);
		try {
			AuthenticationManager.initializeCredentials(clearPassword , cred);
		} catch (Exception e) {
			e.printStackTrace();
		}
		cred=credentialsService.saveInBase(cred);
		client.setCredentials(cred);
		client.setGender(gender);
		client=clientService.saveInBase(client);
		Cart cart = cartService.saveInBase(new Cart());
		cart.setClient(client);
		cart = cartService.saveInBase(cart);
		client.setCart(cart);
		clientService.saveInBase(client);
		message = "Votre compte client a bien été enregistré";
		return "clientMain.xhtml?faces-redirect=true";
	}
	
	public void demo() {
		client.setFirstName("Curie");
		client.setLastName("Marie");
		client.setPhoneNumber("0695687924");
		client.setEmail("mariecurie@eql.com");
		login = "MarieCurie";
		clearPassword="password";
	}

}
